// Създаваме променлива, която съхранява endpoint-a (base) и ключа за автентикация (key)
const api = {
    key: '80a0ff95522315d891606576306ed222',
    base: 'https://api.openweathermap.org/data/2.5/'
};

// Endpoint string = `${api.base}forecast?q=${searchBoxValue}&appid=${api.key}`

// Достъпваме input полето, добавяме listener при натискане на бутона Enter, извикваме getResults()
const searchBox = document.querySelector('.search-box');
searchBox.addEventListener('keypress', function (event) {
    if (event.keyCode == 13 && searchBox.value) {
        getResults(searchBox.value);
    }
});

// Method 1, използвайки fetch().then().catch()
function getResults(query) {
    fetch(`${api.base}forecast?q=${query}&appid=${api.key}`)
        .then(function (response) {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error(response.statusText);
            }
        })
        .then(function (responseJson) {
            showHideError(false);
            displayResults(responseJson);
        })
        .catch(function (error) {
            showHideError(true, error);
        });
}

// Method 2, използвайки async/await и try/catch
// async function getResults(query) {
//     try {
//         const response = await fetch(`${api.base}forecast?q=${query}&appid=${api.key}`);
//         const responseData = await response.json();

//         if (response.ok) {
//             showHideError(false);
//             displayResults(responseData);
//         } else {
//             throw new Error(response.statusText);
//         }
//     } catch (error) {
//         showHideError(true, error.message);
//     }
// }

// Функция, показваща/скриваща съобщението за грешка
function showHideError(show, errorMsg) {
    const errorMsgElement = document.querySelector('#error-msg');
    const dataWrapperElement = document.querySelector('.data-wrapper');

    if (show) {
        errorMsgElement.innerHTML = errorMsg;
        dataWrapperElement.style.display = 'none';
    } else {
        errorMsgElement.innerHTML = '';
        dataWrapperElement.style.display = 'block';
    }
}

// Функция, показваща резултата при status code 200, т.е. при успешна заявка
function displayResults(weather) {
    const cityElement = document.querySelector('.city');
    cityElement.innerHTML = `${weather.city.name}, ${weather.city.country}`;

    const dateElement = document.querySelector('.date');
    const now = new Date();
    dateElement.innerHTML = dateBuilder(now);

    const tempElement = document.querySelector('.temp');
    const currentTemp = kelvinToCelsius(weather.list[0].main.temp);
    tempElement.innerHTML = `${currentTemp}<span>°c</span>`;

    const minMax = document.querySelector('.min-max');
    const minTemp = kelvinToCelsius(weather.list[0].main.temp_min);
    const maxTemp = kelvinToCelsius(weather.list[0].main.temp_max);
    minMax.innerHTML = `${minTemp}°c / ${maxTemp}°c`;
}

// Функция, създаваща стринг, съдържащ деня, датата, месеца и текущата година
function dateBuilder(dateObject) {
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    const day = days[dateObject.getDay()];
    const date = dateObject.getDate();
    const month = months[dateObject.getMonth()];
    const year = dateObject.getFullYear();

    return `${day} ${date} ${month} ${year}`;
}

// Функция, конвертираща Kelvin в Celsius
function kelvinToCelsius(kelvin) {
    return Math.floor(kelvin - 273.15);
}
