const url = 'http://localhost:3000/employees';

const ul = document.getElementById('employees');
const submitBtn = document.getElementById('submitBtn');
const searchBtn = document.getElementById('searchBtn');
const clearBtn = document.getElementById('clearBtn');
const filterIcon = document.getElementById('filterIcon');
const searchInput = document.getElementById('search');
const sortByAscBtn = document.getElementById('sortByAsc');
const sortByDescBtn = document.getElementById('sortByDesc');
const firstNameInput = document.getElementById('firstName');
const lastNameInput = document.getElementById('lastName');
const phoneInput = document.getElementById('phone');

let showSearchForm = false;
let currentEmployee = {};

function createNode(element) {
    return document.createElement(element);
}

function append(parent, el) {
    return parent.appendChild(el);
}

async function getEmployees(url) {
    const response = await fetch(url);
    const responseData = await response.json();

    try {
        if (response.ok) {
            displayEmployees(responseData);
        } else {
            throw new Error(response.statusText);
        }
    } catch (error) {
        console.log(error);
    }
}

function displayEmployees(employees) {
    ul.innerHTML = '';

    if (!employees.length) {
        const noData = createNode('div');
        noData.innerHTML = 'No employees. Please add.';
        append(ul, noData);

        return;
    }

    employees.forEach(function (employee) {
        const li = createNode('li');
        const firstName = createNode('div');
        const lastName = createNode('div');
        const phone = createNode('div');
        const updateBtn = createNode('a');
        const deleteBtn = createNode('a');

        updateBtn.href = '#';
        updateBtn.innerHTML = '[Edit]';
        updateBtn.addEventListener('click', function (event) {
            event.preventDefault();
            updateEmployee(employee.id);
        });

        deleteBtn.href = '#';
        deleteBtn.innerHTML = '[Delete]';
        deleteBtn.addEventListener('click', function (event) {
            event.preventDefault();
            deleteEmployee(employee.id);
        });

        firstName.innerHTML = `First name: ${employee.firstName}`;
        lastName.innerHTML = `Last name: ${employee.lastName}`;
        phone.innerHTML = `Phone: ${employee.phone}`;

        append(li, firstName);
        append(li, lastName);
        append(li, phone);
        append(li, updateBtn);
        append(li, deleteBtn);
        append(ul, li);
    });
}

async function saveEmployee() {
    const form = document.getElementById('saveEmployeeForm');
    const firstNameVal = firstNameInput.value;
    const lastNameVal = lastNameInput.value;
    const phoneVal = phone.value;
    let response;

    if (!firstNameVal || !lastNameVal || !phoneVal) {
        alert('All fields are required.');
        return;
    }

    const data = {
        firstName: firstNameVal,
        lastName: lastNameVal,
        phone: phoneVal
    };

    if (!Object.keys(currentEmployee).length) {
        response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        });
    } else {
        response = await fetch(`${url}/${currentEmployee.id}`, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        });
    }

    try {
        if (response.ok) {
            getEmployees(url);
            currentEmployee = {};
        } else {
            throw new Error(response.statusText);
        }
    } catch (error) {
        console.log(error);
    }

    form.reset();
}

async function updateEmployee(employeeId) {
    const response = await fetch(`${url}/${employeeId}`, {
        method: 'GET'
    });
    const employee = await response.json();

    firstNameInput.value = employee.firstName;
    lastNameInput.value = employee.lastName;
    phoneInput.value = employee.phone;
    currentEmployee = employee;
}

async function deleteEmployee(employeeId) {
    const response = await fetch(`${url}/${employeeId}`, {
        method: 'DELETE'
    });

    try {
        if (response.ok) {
            getEmployees(url);
        } else {
            throw new Error(response.statusText);
        }
    } catch (error) {
        console.log(error);
    }
}

function searchByFirstName() {
    const searchInputVal = searchInput.value;

    if (!searchInputVal) {
        alert('Please enter a value.');
        return;
    }

    getEmployees(`${url}?firstName=${searchInputVal}`);
}

function showHideSearchForm() {
    const searchForm = document.getElementById('searchForm');

    if (showSearchForm) {
        searchForm.style.display = 'block';
    } else {
        searchForm.style.display = 'none';
    }

    showSearchForm = !showSearchForm;
}

function sortEmployees(order) {
    getEmployees(`${url}?_sort=firstName&_order=${order}`);
}

// Add Event listeners
submitBtn.addEventListener('click', function (event) {
    event.preventDefault();
    saveEmployee();
});

filterIcon.addEventListener('click', function (event) {
    event.preventDefault();
    showHideSearchForm();
});

searchBtn.addEventListener('click', function (event) {
    event.preventDefault();
    searchByFirstName();
});

clearBtn.addEventListener('click', function (event) {
    event.preventDefault();

    if (searchInput.value) {
        getEmployees(url);
    }

    searchInput.value = '';
});

sortByAscBtn.addEventListener('click', function (event) {
    event.preventDefault();
    sortEmployees('asc');
});

sortByDescBtn.addEventListener('click', function (event) {
    event.preventDefault();
    sortEmployees('desc');
});

getEmployees(url);
showHideSearchForm();
