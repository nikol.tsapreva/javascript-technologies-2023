function addItem() {
    const input = document.getElementById('input');
    const err = document.getElementById('errorMsg');

    // Проверяваме дали стойността input-a е празна и ако е - прекратяваме изпълнението на фукнцията с return, извеждайки подходящо валидационно съобщение
    if (!input.value) {
        err.style.color = 'red';
        err.innerHTML = 'Enter value in the input.';

        return;
    }

    // Премахваме стойността на errorMsg полето
    err.innerHTML = '';

    // Достъпваме нашия списък
    const items = document.getElementById('items');

    // Създаваме нов елемент li и го записваме в променлива
    const newli = document.createElement('li');

    // Приравняваме съдържанието на този елемент на стойността на input-a
    newli.innerHTML = input.value;

    // Създаваме елемент със съдържание [Delete]
    const deleteLink = document.createElement('a');
    deleteLink.innerHTML = '[Delete]';
    deleteLink.href = '#';

    // Добавяме събитие при клик на елемента [Delete], което ще премахне елемента от DOM
    deleteLink.addEventListener('click', function () {
        items.removeChild(newli);
    });

    // Добавяме [Delete] към новия li
    newli.appendChild(deleteLink);

    // Към списъка добавяме новия li елемент с appendChild()
    items.appendChild(newli);

    // Премахваме стойността на input полето
    input.value = '';
}
