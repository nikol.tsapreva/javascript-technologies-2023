// Достъпваме всички елементи от DOM, които ще са ни необходими.
const bill = document.getElementById('bill');
const serviceQuality = document.getElementById('serviceQuality');
const numOfPeople = document.getElementById('people');
const tip = document.getElementById('tip');
const errorMsg = document.getElementById('errorMsg');
errorMsg.style.color = 'red';

// Създаваме функция за валидиране на полетата - bill, serviceQuality, numOfPeople
function validateInputs() {
    if (!bill.value || serviceQuality.value == 0 || !numOfPeople.value) {
        errorMsg.innerHTML = 'Всички полета са задължителени, попълнете ги.';
        return false;
    }

    if (bill.value <= 0) {
        errorMsg.innerHTML = 'Сметката трябва да е поне 0.01лв.';
        return false;
    }

    if (numOfPeople.value <= 0) {
        errorMsg.innerHTML = 'В поделянето на сметката трябва да присъства поне 1 човек.';
        return false;
    }

    return true;
}

function calculateTip() {
    // Проверяваме какво връща валидиращата функция. Ако е false - спираме изпълнението на calculateTip, в противен случай не
    const areInputsValid = validateInputs();
    if (!areInputsValid) {
        return;
    }

    // Зануляваме стойността и стиловете на errorMsg
    errorMsg.innerHTML = '';

    // Калкулираме бакшиша - (умножаваме сметката по стойността на обслужването), разделено на броя на клиентите
    const total = (Number(bill.value) * Number(serviceQuality.value / 100)) / Number(numOfPeople.value);

    // Показваме резултата от изчисленията, за да получим бакшиша
    tip.innerHTML = `По ${total.toFixed(2)}лв на човек`;
}
