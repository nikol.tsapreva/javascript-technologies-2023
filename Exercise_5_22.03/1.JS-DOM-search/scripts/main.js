const inputEl = document.getElementById('userInput');

function search() {
    const text = document.getElementById('text');
    const listItems = document.getElementsByTagName('li');
    let br = 0;

    for (let i = 0; i < listItems.length; i++) {
        const content = listItems[i].innerHTML.toLowerCase();k
        const inputValLowerCase = inputEl.value.toLowerCase();

        if (inputEl.value && content.includes(inputValLowerCase)) {
            listItems[i].style.fontWeight = 'bold';
            br++;
        } else {
            listItems[i].style.fontWeight = 'normal';
        }
    }

    if (!inputEl.value) {
        text.style.color = 'red';
        text.innerHTML = 'Enter value in the input.';
        inputEl.style.borderColor = 'red';
    } else {
        text.style.color = 'green';
        text.innerHTML = `${br} matches found`;
        inputEl.style.borderColor = 'black';
    }
}

// Add event listener from JS
inputEl.addEventListener('keypress', function (event) {
    if (event.key === 'Enter') {
        event.preventDefault();
        search();
    }
});
