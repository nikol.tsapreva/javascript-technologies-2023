function updateContent() {
    // Достъпваме всички необходими елементи
    const rowInput = document.getElementById('row');
    const rowVal = parseInt(rowInput.value);
    const rowError = document.getElementById('row-error');
    const colInput = document.getElementById('column');
    const colVal = parseInt(colInput.value);
    const colErr = document.getElementById('column-error');
    const contentVal = document.getElementById('content').value;
    const table = document.getElementById('my-table');
    let isError = false;

    // Валидираме дали rowVal е число, дали е между 1 и 3, тъй като редовете са с три
    if (isNaN(rowVal) || rowVal < 1 || rowVal > 3) {
        isError = true;
        rowError.style.color = 'red';
        rowError.innerHTML = 'Enter a number between 1 and 3.';
    } else {
        rowError.innerHTML = '';
    }

    // Валидираме дали colVal е число, дали е между 1 и 2, тъй като клетките на всеки ред (колоните) са две
    if (isNaN(colVal) || colVal < 1 || colVal > 2) {
        isError = true;
        colErr.style.color = 'red';
        colErr.innerHTML = 'Enter a number between 1 and 2.';
    } else {
        colErr.innerHTML = '';
    }

    if (isError) {
        return;
    }

    rowInput.value = '';
    colInput.value = '';
    content.value = '';

    // Достъпваме клетките за съответния ред, където rowVal e стойността, въведена от потребителя, а с rowVal - 1 достъпваме индекса на реда, тъй като индексът започва от 0
    const cellsOnRow = table.rows[rowVal - 1].cells;

    // Променяме стойността на съответната клетка, където colVal e стойността, въведена от потребителя, а с colVal - 1 достъпваме индекса на клетката, тъй като индексът започва от 0
    cellsOnRow[colVal - 1].innerHTML = contentVal;

    // Създаваме successMsgEl елемент
    const successMsgEl = document.createElement('span');

    // Добавяме съдържание и множество от стилове за successMsgEl, след това го добавяме към body
    successMsgEl.innerHTML = 'The content has been succesfully updated.';
    successMsgEl.style.cssText = 'position: absolute; right: 8px; top: 8px; background-color: green; color: white; padding: 20px;';
    document.body.appendChild(successMsgEl);

    // Изтриваме successMsgEl след 3 секунди
    setTimeout(function () {
        document.body.removeChild(successMsgEl);
    }, 3000);
}
