// Достъпваме #main-table елемента
const table = document.getElementById('main-table');

// Функция за добавяне на нов ред в таблицата
function addRow() {
    // Достъпваме #firstName, #lastName, #error елементите
    const firstName = document.getElementById('firstName');
    const lastName = document.getElementById('lastName');
    const error = document.getElementById('error');

    // Проверяваме дали поне едно от полетата няма стойност. Ако да, извеждаме валидационното съобщение и спираме изпълнението.
    if (!firstName.value || !lastName.value) {
        error.innerHTML = 'All fields are required.';
        error.style.color = 'red';
        return;
    }

    error.innerHTML = '';

    // Вземаме броя на редовете в таблицата и вмъкваме нов на следващата позиция.
    // Ако table.rows = [0, 1] -> length = 2, вмъкваме следващ ред с индекс 2
    const rowCount = table.rows.length;
    const row = table.insertRow(rowCount);

    // За двете клетки (колони) за съотвения ред вмъкваме съоветните стойности
    row.insertCell(0).innerHTML = firstName.value;
    row.insertCell(1).innerHTML = lastName.value;

    // За третата клетка създаваме бутон, който ще изтрива съответния ред
    const deleteBtn = document.createElement('button');
    deleteBtn.innerHTML = 'Delete';
    deleteBtn.addEventListener('click', function () {
        deleteRow(this);
    });

    // Добавяме бутона в третата клетка на реда
    row.insertCell(2).appendChild(deleteBtn);

    firstName.value = '';
    lastName.value = '';
}

// Функция за изтриване на ред от таблицата
function deleteRow(obj) {
    // Достъпваме индекса на реда и го изтриваме посредством deleteRow(index)
    const index = obj.parentNode.parentNode.rowIndex;
    table.deleteRow(index);
}
