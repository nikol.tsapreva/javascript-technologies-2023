// 1. Да се напише програма, която извежда "Hello world!" наобратно. Резултатът да се изведе в конзолата.
const myString = 'Hello world!';

// Метод 1
let myReversedString1 = '';
for (let i = myString.length - 1; i >= 0; i--) {
  myReversedString1 += myString[i];
}
console.log(myReversedString1);

// Метод 2
let myReversedString2 = '';
myReversedString2 = myString.split('').reverse().join('');
console.log(myReversedString2);

// 2. Проверете дали string "Foo" съдържа substring "oo". Резултатът да се изведе в конзолата.
const stringFoo = 'Foo';
const substringFoo = 'oo';

// Метод 1
const isPartOfStr1 = stringFoo.includes(substringFoo);
console.log(isPartOfStr1);

// Метод 2
const isPartOfStr2 = stringFoo.indexOf(substringFoo) !== -1;
console.log(isPartOfStr2);

// 3. Заменете "Microsoft" от дадения стринг с "W3Schools", но с главни букви. Резултатът да се изведе в конзолата.
let str = 'Please visit Microsoft!';
const replacingWord = 'W3Schools';
const upperCaseWord = replacingWord.toUpperCase();
str = str.replace('Microsoft', upperCaseWord);
console.log(str);
