// 1 задача:
// Да се създаде обект user, който има следните свойства: firstName, lastName, age, fullName ф-я (връща firstName + lastName), yearOfBirth ф-я (връща годината на раждане на user-a на база age и текущата година).
const user = {
    firstName: 'Nikol',
    lastName: 'Tsapreva',
    age: 26,
    fullName: function () {
        return this.firstName + ' ' + this.lastName;
    },
    yearOfBirth: function () {
        const currentYear = new Date().getFullYear();
        const yearOfBirth = currentYear - this.age;

        return yearOfBirth;
    }
};

// Достъпване на firstName property
user.firstName;
user['firstName'];

// Изтриване на firstName property
delete user.firstName;

// Извикваме fullName ф-ята
console.log(user.yearOfBirth());

// Обхождане на обекта user и извеждане в конзолата на key - value pairs
for (const key in user) {
    console.log(`${key}: ${user[key]}`);
}

// 2 задача:
// Създаване на клас User и обект от този клас; properties - firstName, lastName, age; methods - getFullName, getYearOfBirth
class User {
    constructor(firstName, lastName, age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    getFullName() {
        return this.firstName + ' ' + this.lastName;
    }

    getYearOfBirth() {
        const currentYear = new Date().getFullYear();
        const yearOfBirth = currentYear - this.age;

        return yearOfBirth;
    }
}

const userIvan = new User('Ivan', 'Ivanov', 24);
console.log(userIvan);
