// 1 задача:
// Напишете JS функция, която приема два параметъра - стринг и буква/цифра.
// Функцията трябва да върне колко пъти сe среща съответната буква/цифра в стринга.
function charCount(str, letter) {
    let letterCount = 0; // Създаваме променлива, в която ще пазим колко пъти се среща дадената буква/цифра

    // Обхождаме str
    for (let position = 0; position < str.length; position++) {
        // Проверяваме дали буквата на съответната позиция съвпада. Ако да - увеличаваме стойността на променливата letterCount
        if (str.charAt(position) === letter) {
            letterCount += 1;
        }
    }

    return letterCount; // Връщаме колко пъти се среща буквата/цифрата в дадената дума
}

const resultCharCount = charCount('w3resource.com', 'o');
console.log('resultCharCount: ', resultCharCount);

// 2 задача:
// Напишете JS функция, която приема два параметъра - стринг и търсен стринг.
// Функцията трябва да премахне първата поява на търсения стринг.
function removeFirstOccurrence(str, searchStr) {
    // Създаваме променлива, което ще връща дали търсеният стринг е част от посочения. Ако е - то ще връща позицията, в противен случай: -1
    const index = str.indexOf(searchStr);

    // Ако търсеният стринг не е част от посочения - изпълнението на ф-ята приключва, връщайки стринга, в който търсим
    if (index === -1) {
        return str;
    }

    // Ако searchStr се съдържа в str и знаем на коя позиция е - чрез метода slice разрязваме str и премахваме searchStr
    return str.slice(0, index) + str.slice(index + searchStr.length);
}

const resultRemoveFirstOccurrence = removeFirstOccurrence('the laptop is not broken', 'not');
console.log(resultRemoveFirstOccurrence);
