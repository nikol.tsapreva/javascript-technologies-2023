// За дадения масив извършете следните действия:
const numbers = [10, 20, 30, 40, 50, 75, 125];

// 1. Обходете масива и изведете всички елементи в конзолата, използайки forEach().
numbers.forEach(function (number) {
    console.log(number);
});

// 2. Обходете масива, като изведете индикация кое число е четно и кое не е (презаписване на стойността му), използвайки map() - връща нов масив.
const newArr = numbers.map(function (number) {
    if (number % 2 === 0) {
        return (number = `${number} е четно число`);
    } else {
        return (number = `${number} е нечетно число`);
    }
});
console.log(newArr);

// 3. Изчислете средно аритметичното на числата в масива, използвайки reduce().
const average =
    numbers.reduce(function (a, b) {
        return a + b;
    }, 0) / numbers.length;
console.log(average);

// 4. Филтрирайте масива, използайки filter() - филтрираният масив да съдържа числата по-големи от 25.
const filteredArr = numbers.filter(function (number) {
    return number > 25;
});
console.log(filteredArr);
