function notify(message) {
    // Запазваме в променлива елемента за известие, достъпвайки го чрез id
    const notification = document.getElementById('notification');

    // Достъпваме съдържанието на известието с innerHTML, което става равно на message
    notification.innerHTML = message;

    // Променяме стила на display от none на block, което ще направи известието видимо
    notification.style.display = 'block';

    // Чрез setTimeout задаваме функция, която ще се изпълни след определен интервал от време - след 2 секунди скриваме известието
    setTimeout(function () {
        notification.style.display = 'none';
    }, 2000);
}
