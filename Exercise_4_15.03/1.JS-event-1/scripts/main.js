function findDuplicatesInArray(arr) {
    const paragraph = document.getElementById('paragraph'); // Достъпваме <p> елемента
    const object = {}; // Създаваме обект, в който ще пазим броя на срещанията на съответните елементи в масива
    const result = []; // Създаваме масив, който ще съхраняваме повтарящите се елементи и ще върнем в края на функцията

    // С помощта на for цикъл обхождаме arr
    for (let i = 0; i < arr.length; i++) {
        const item = arr[i];

        // Ако нямаме key за съответния елемент от масива, то създаваме такъв с брой 0
        if (!object.hasOwnProperty(item)) {
            object[item] = 0;
        }

        object[item] += 1; // Увеличаваме броя на записите с 1
    }

    // С помощта на for цикъл обхождаме всички записи в обекта
    for (const prop in object) {
        // Ако броят на съответния запис е по-голям или равен на 2 - добавяме го в result масива, т.е. елементът се повтаря
        if (object[prop] >= 2) {
            result.push(parseInt(prop));
        }
    }

    result.sort(function (a, b) {
        return a - b;
    });

    paragraph.innerHTML = result;
}
