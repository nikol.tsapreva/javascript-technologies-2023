// Типове данни
// Primitive types: null, undefined, string, boolean, number
// Reference types: functions, arrays, object - от "Object"
const x = 5;
const lastName = 'Ivanov';
const isActive = true;
const userData = { name: 'Ivan', age: 18 };
const cars = ['Audi', 'VW', 'Fiat'];

// typeOf операторът връща типа на променливата
console.log(typeof userData);

// Опасност от загубване на типове данни
const johnTaskCount = 11;
const janeTaskCount = '42';
const totalTaskCount = johnTaskCount + janeTaskCount;
console.log(totalTaskCount); // "1142" - string
