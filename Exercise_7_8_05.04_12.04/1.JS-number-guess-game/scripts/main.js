// Достъпваме всички необходими DOM елементи
const submit = document.querySelector('#submit');
const userInput = document.querySelector('#guess-field');
const guessSlot = document.querySelector('.guesses');
const remaining = document.querySelector('.last-result');
const resultWrapper = document.querySelector('.result-wrapper');
const lowOrHigh = document.querySelector('.low-or-high');

// Помощни променливи
let numGuesses = 10;
let randomNumber = generateRandomNumber();
let previousGuesses = [];

// Ф-я, генерираща случайно число от 1 до 100
function generateRandomNumber() {
    return Math.floor(Math.random() * 100 + 1);
}

// Ф-я, валидираща стойността на input полето със съответните валидационни alert съобщения
function validateGuess(guess) {
    if (isNaN(guess)) {
        alert('Please enter a valid number');
        return false;
    }

    if (guess < 1) {
        alert('Please enter a number greater than 1!');
        return false;
    }

    if (guess > 100) {
        alert('Please enter a number less than 100!');
        return false;
    }

    return true;
}

// Ф-я, показваща всички предположения и колко такива остават
function displayGuesses(guess) {
    userInput.value = '';
    previousGuesses.push(guess);
    guessSlot.innerHTML = previousGuesses;
    numGuesses--;
    remaining.innerHTML = numGuesses;
}

function checkGuess(guess) {
    // Показване на подсказка - дали числото, въведено от потребителя е по-голямо или по-малко от първоначално генерираното
    // Ако съвпадат - извеждане на съобщение за "отгатване" и край на играта
    if (guess === randomNumber) {
        displayMessage('You guessed correctly!');
        endGame();
    } else if (guess < randomNumber) {
        displayMessage('Too low! Try again!');
    } else if (guess > randomNumber) {
        displayMessage('Too High! Try again!');
    }
}

// Ф-я, извеждаваща съобщение
function displayMessage(message) {
    lowOrHigh.innerHTML = message;
}

// Ф-я за прекратяване на играта
function endGame() {
    userInput.value = '';

    // Неактивен input и submit - disabled
    userInput.setAttribute('disabled', '');
    submit.setAttribute('disabled', '');

    // Създаване на Start New Game бутон
    const startNewGameBtn = document.createElement('button');
    startNewGameBtn.setAttribute('id', 'submit');
    startNewGameBtn.className = 'new-game-btn';
    startNewGameBtn.innerHTML = 'Start new game';

    // Добавяме бутона като дете на resultWrapper
    resultWrapper.appendChild(startNewGameBtn);

    // Стартираща нова игра, генерираща ново случайно число и зануляване стойностите на променливите
    startNewGameBtn.addEventListener('click', function () {
        randomNumber = generateRandomNumber();
        numGuesses = 10;
        previousGuesses = [];
        guessSlot.innerHTML = '';
        lowOrHigh.innerHTML = '';
        remaining.innerHTML = numGuesses;
        userInput.removeAttribute('disabled');
        submit.removeAttribute('disabled', '');
        resultWrapper.removeChild(startNewGameBtn);
    });
}

submit.addEventListener('click', function (e) {
    // Премахваме поведението по подразбиране на браузъра - рефреш
    e.preventDefault();

    // Достъпване на "предположението" на потребителя
    const guess = parseInt(userInput.value);

    // Валидираме стойността на input полето, която е въвел потребителят
    const isValid = validateGuess(guess);

    if (!isValid) {
        return;
    }

    // Проверяваме дали играта е приключила, т.е. дали потребителят е въвел 10 числа.
    if (numGuesses < 1) {
        userInput.value = '';
        displayMessage(`Game Over! The number was ${randomNumber}`);
        endGame();
    } else {
        displayGuesses(guess);
        checkGuess(guess);
    }
});
